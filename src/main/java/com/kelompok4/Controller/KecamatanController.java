package com.kelompok4.Controller;

import com.kelompok4.model.dto.KecamatanDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/kecamatan")
public class KecamatanController {

	@GetMapping("/")
	public List<KecamatanDto> get() {
		return kecamatan();
	}

	@GetMapping("/{code}")
	public KecamatanDto get(@PathVariable String code) {
		List<KecamatanDto> kecamatanList = kecamatan();
		for (KecamatanDto kecamatanDto : kecamatanList) {
			if (kecamatanDto.getCodeKec().equalsIgnoreCase(code)) {
				return kecamatanDto;
			}
		}
		return null;
	}

	public List<KecamatanDto> kecamatan() {
		List<KecamatanDto> kecamatanList = new ArrayList<>();

		KecamatanDto angs = new KecamatanDto();
		angs.setCodeKec("360107");
		angs.setNameKec("Angsana");
		kecamatanList.add(angs);

		KecamatanDto banj = new KecamatanDto();
		banj.setCodeKec("360120");
		banj.setNameKec("Banjar");
		kecamatanList.add(banj);

		return kecamatanList;
	}

}
