package com.kelompok4.Controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kelompok4.model.dto.ProvinceDto;


@RestController
@RequestMapping("/province")
public class ProvinceController {
	
	@GetMapping("/")
	public List<ProvinceDto> get() {
		return province();
	}
	@GetMapping("/{code}")
	public ProvinceDto get(@PathVariable String code) {
		List<ProvinceDto> provinceList = province();
		for (ProvinceDto provinceDto : provinceList) {
			if (provinceDto.getCode().equalsIgnoreCase(code)) {
				return provinceDto;
			}
		}
		return null;
	}
	public List<ProvinceDto> province() {
		List<ProvinceDto> provinceList = new ArrayList<>();
		
		ProvinceDto banten = new ProvinceDto();
		banten.setCode("01");
		banten.setName("banten");
		provinceList.add(banten);
		
		ProvinceDto jabar = new ProvinceDto();
		jabar.setCode("02");
		jabar.setName("jabar");
		provinceList.add(jabar);
		
		return provinceList;
	}
}
