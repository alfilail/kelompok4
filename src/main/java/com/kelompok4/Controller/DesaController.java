package com.kelompok4.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kelompok4.model.dto.DesaDto;

@RestController
@RequestMapping("/desa")
public class DesaController {

	@GetMapping("/")
	public List<DesaDto> get() {
		return generateDesa();
	}

	public List<DesaDto> generateDesa() {
		List<DesaDto> dlist = new ArrayList<>();

		DesaDto banten = new DesaDto();
		banten.setCodeDesa("036.74.04.1003");
		banten.setDesa("Ciputat");
		dlist.add(banten);
		
		DesaDto jabar = new DesaDto();
		jabar.setCodeDesa("32.17.08.2001");
		jabar.setDesa("Kertamulya");
		dlist.add(jabar);

		return dlist;
	}

	@GetMapping("/{code}")
	public DesaDto getList(@PathVariable String code) {
		List<DesaDto> dlist = generateDesa();
		for (DesaDto desaDto : dlist) {
			if (desaDto.getCodeDesa().equalsIgnoreCase(code)) {
				return desaDto;
			}
		}
		return null;
	}
}
