package com.kelompok4.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kelompok4.model.dto.KabupatenDTO;


import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping ("/Kabupaten")

public class KabupatenController {
	
	@GetMapping("/")
	public List<KabupatenDTO> get() {
		return kabupaten();
	}
	
	@GetMapping("/{code}")
	public KabupatenDTO get(@PathVariable String code) {
		List<KabupatenDTO> kabupatenList = kabupaten();
		for (KabupatenDTO kabupatenDto : kabupatenList) {
			if (kabupatenDto.getCodeKabupaten().equalsIgnoreCase(code)) {
				return kabupatenDto;
			}
		}
		return null;
	}
	
	public List<KabupatenDTO> kabupaten(){
		List<KabupatenDTO> kabupatenList = new ArrayList<>();
		
		KabupatenDTO pandeglang = new KabupatenDTO();
		pandeglang.setCodeKabupaten("36.01");
		pandeglang.setNamaKabupaten("Pandegelang");
		kabupatenList.add(pandeglang);
		
		KabupatenDTO bandung = new KabupatenDTO();
		bandung.setCodeKabupaten("32.04");
		bandung.setNamaKabupaten("Bandung");
		kabupatenList.add(bandung);
		
		return kabupatenList;
	}
}
