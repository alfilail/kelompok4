package com.kelompok4.model.dto;

public class KabupatenDTO {
	private String NamaKabupaten;
	private String CodeKabupaten;

	public String getNamaKabupaten() {
		return NamaKabupaten;
	}

	public void setNamaKabupaten(String NamaKabupaten) {
		this.NamaKabupaten = NamaKabupaten;
	}

	public String getCodeKabupaten() {
		return CodeKabupaten;
	}

	public void setCodeKabupaten(String CodeKabupaten) {
		this.CodeKabupaten = CodeKabupaten;
	}

}
