package com.kelompok4.model.dto;

public class DesaDto {

	private String Desa;
	private String codeDesa;

	public String getDesa() {
		return Desa;
	}

	public void setDesa(String name) {
		this.Desa = name;
	}

	public String getCodeDesa() {
		return codeDesa;
	}

	public void setCodeDesa(String code) {
		this.codeDesa = code;
	}

}
