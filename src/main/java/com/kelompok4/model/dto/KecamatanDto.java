package com.kelompok4.model.dto;

public class KecamatanDto {
	private String codeKec;
	private String Kecamatan;

	public String getCodeKec() {
		return codeKec;
	}

	public void setCodeKec(String codeKec) {
		this.codeKec = codeKec;
	}

	public String getNameKec() {
		return Kecamatan;
	}

	public void setNameKec(String nameKec) {
		this.Kecamatan = nameKec;
	}
}
